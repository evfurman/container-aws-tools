FROM alpine:edge

RUN apk add --update --no-cache \
  bash \
  ca-certificates \
  curl \
  docker \
  go \
  git \
  jq \
  musl-dev \
  nodejs-npm \
  openssh \
  py-pip \
  rsync \
  && rm -rf /var/cache/apk/*

RUN pip install awscli
RUN go get -v github.com/bep/s3deploy
RUN npm install -g gitlab-ci-variables-cli
RUN cp /root/go/bin/s3deploy /usr/bin/
